// Constants
const token = require("../auth.json").token;
const discord = require("../node_modules/discord.js");
const express = require("../node_modules/express");
const fs = require("fs");
const request = require("../node_modules/request");
const cheerio = require("../node_modules/cheerio");

const scraper = express();

var userignores = [];
var chnlignores = [];

const bot = new discord.Client({
    disableEveryone: true
});

// Basic Functions
function log(msg){
    console.log("[LOG] " + msg);
};
String.prototype.replaceAll = function(str1, str2, ignore){
    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)==="string")?str2.replace(/\$/g,"$$$$"):str2);
};

// When bot connects to server
bot.on("ready", function(){
    log("Bot connected as: " + bot.user.username + " (" + bot.user.id + ")");
    bot.user.setGame('defbot_help');
    try{
        userignores = fs.readFileSync(__dirname + "/../userignores.txt").toString().split("\n");
        chnlignores = fs.readFileSync(__dirname + "/../chnlignores.txt").toString().split("\n");
    }catch(ex){
        log("Issue reading \""+__dirname+"/../userignores.txt\"");
        log("Issue reading \""+__dirname+"/../chnlignores.txt\"");
    }
});

bot.login(token);

bot.on("message", message => {
    var replacenospace = ["1","2","3","4","5","6","7","8","9","0"];
    var replacespace = ['!','@','#','$','%','^','&','*','(',')','-','_','+','=',"[","]","{","}","\\\\","|",";","\n",";",":","\"","'","<",">",",",".","/","?","`","~","\t","\v","\r"];
    var noignore = true;
    if(userignores.includes(message.author.id) || chnlignores.includes(message.channel.id)) noignore = false; 
    if(message.content.toLowerCase()==="defbot_chnlignore"){
        if(noignore){
            chnlignores.push(message.channel.id);
            message.channel.send("Lowering the average IQ of this channel by 10%...\nProcess completed.");
        }
        else{
            var index = chnlignores.indexOf(message.channel.id);
            if(index > -1){
                chnlignores.splice(index,1);
            }
            message.channel.send("Repentance: the action or process of repenting especially for misdeeds or moral shortcomings, such as blocking me from this channel. I'm back.");
        }
        writeFiles();
    }
    else if(message.content.toLowerCase()==="defbot_userignore"){
        if(noignore){
            userignores.push(message.author.id);
            message.channel.send("So you think you don't need me anymore? Good luck on your next paper.");
        }
        else{
            var index = userignores.indexOf(message.author.id);
            if(index > -1){
                userignores.splice(index,1);
            }
            message.channel.send("You came crawling back. Was your lexicon not strong enough?");
        }
        writeFiles();
    }
    else if(message.content.toLowerCase()==="defbot_help"){
        message.channel.send("I automagically read sophisticated messages to explain the denotations of your advanced vocabulary. I am sure someone as perspicacious as you would understand.\n"
        +"My commands include:\n"
        +"  - defbot_chnlignore - Declare I stop listening to the current channel\n"
        +"  - defbot_userignore - Declare I stop listening to the user who spoke this message.");
    }
    else if(!message.author.bot && !message.content.toLowerCase().includes("http://") && !message.content.toLowerCase().includes("https://") && noignore){
        var msg = message.content;
        var out = "";
        replacespace.forEach(str => {
            msg = msg.replaceAll(str," ");
        });
        msg.split(" ").forEach(s => {
            replacenospace.forEach(function(str){
                s = s.replaceAll(str,"");
            });
            if(s.length >= 10 && !s.startsWith("<@!")){
                async function scrape(out,s){
                    var def = "";
                    var url = "http://merriam-webster.com/dictionary/" + s;
                    await request(url, function(error, response, html){
                        if(!error){
                            var $ = cheerio.load(html);
                            var l = 0;
                            $('span.dt').each(function(i,element){
                                if(l===0){
                                    var data = $(this).text();
                                    data = data.substring(data.indexOf(":")+2);
                                    data = data.substring(0,data.indexOf("     "));
                                    log(data);
                                    def = data;
                                    out = out + s + ": " + def;
                                    message.channel.send(out);
                                    l = 1;
                                }
                            });
                            if(l===0){
                                def = "Error! Definition not found!";
                                out = out + s + ": " + def;
                                message.channel.send(out);
                            }
                        }else{
                            def = "Error searching for word.";
                            out = out + s + ": " + def;
                            message.channel.send(out);
                        }
                    });
                }
                scrape(out,s);
            }
        });
    }
});

function writeFiles(){
    var ui="",ci=""; // user ignore & channel ignore temp variable
    userignores.forEach(str => {
        ui = ui + str + "\n";
    });
    chnlignores.forEach(str => {
        ci = ci + str + "\n";
    });
    try{
        fs.writeFileSync(__dirname + "/../userignores.txt",ui);
        fs.writeFileSync(__dirname + "/../chnlignores.txt",ci);
    }catch(ex){
        log("Issue reading \""+__dirname+"/../userignores.txt\"");
        log("Issue reading \""+__dirname+"/../chnlignores.txt\"");
    }
}

